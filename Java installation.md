# JAVA INSTALLATION:

**Note: This part of the exercise has been completed while doing Hadoop Lab, so if you have Java 1.7 and Hadoop installed in the system then you can skip both of these step and jump to Apache Pig Download part.**

Check before downloading it because mostly this version is pre-installed in every system.

Go to command prompt and type command **java -version**.

if not installed then follow the below steps.

**Step 1:** Click on [Download java](https://www.oracle.com/java/technologies/javase/javase7-archive-downloads.html)

Select you operating system version and click on **jdk** link.

![java download](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_1.7_version__2_.png)


**Step 2:** After clicking on jdk link you will get this option shown below, just accept it and select download option.

![oracle signup](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_download_oracle_signin.png)

**Step 3:** After clicking on download option you will be directed to oracle login page. Create your account and sign in.

As you sign in, java jdk file will start downloading.

![signup](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_oracle.png)

**Step 4:** Click on downloaded **jdk** file and select “Next”

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_1.png)

**Step 5:** Click on **“Next”** and check it must be installed in C:\ drive only.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_2.png)

**Step 6:** Then Click **“Change”** and in C:\ drive, create new folder **“JAVA or similar name”** and click on “Next”.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_4.png)

**Step 7:** Java starts installing.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_5.png)

**Step 8:** Go to **“Program files"** in C:\ drive, you will find a folder named there as “java” open it and copy the jdk folder from there.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_6.png)

**Step 9:** Paste this jdk file in the folder created at installation time “JAVA’’ in C:\ drive. Open this JAVA folder and paste **jdk** file here.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_7.png)

**Step 10:** **NOTE:** Delete that java folder from program files in C:\ drive. There should be only one java file.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java.png)

### Environmental variables setting for JAVA.

**Step 1:** Click on start and go to **settings** --> In setting go to **“system”** and then search for **“edit for environment variables”** --> Select **“environment variables”** and click ok --> Click on **“New”** in User Variables.

Write variable name as: “JAVA_HOME” and variable value is the path location of jdk bin.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_8.png)

**Step 2:** How to get the path location of jdk bin.

Go to JAVA folder in C: drive --> open jdk1.7.0_80 file --> open bin. Then copy the path.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_9.png)

**Step 3:** Double Click on **path** in System Variables --> This opens path, click on **“New”** and then paste the path location of jdk bin here also. Then click **ok** on all open pages. Now you are done with setting up environment variables

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_10.png)


### Veryfying the installation of JAVA.(COMMAND PROMPT)

**Step 1:** Open Command prompt and type **“javac”** and enter.

![cmd](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/cmd_java.png)

**Step 2:** To know the version of java installed type **“java -version”**. It will appear like this.

![cmd](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/cmd_java_1.png)




