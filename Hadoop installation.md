# HADOOP INSTALLATION

**Note: This part of the exercise has been completed while doing Hadoop Lab, so if you have Java 1.7 and Hadoop installed in the system then you can skip both of these step and jump to Apache Pig Download part.**

### HADOOP DOWNLOAD:

**Step 1:** Click on [Download hadoop](https://hadoop.apache.org/release/2.4.1.html) 

Select **Download tar.gz** file(green box) and hadoop tar file will be dowanloaded.

![hadoop](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop.png)

**NOTE:** if there is no option like WinRAR then download it from this link [WinRAR](https://www.win-rar.com/predownload.html?&L=0) 

![winrar](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/winrar.png)

**Step 2:** Extract the downloaded file using WinRAR.

when you click on extract file this extraction path and options page will open.

Select C:\ drive and make new folder named as “HADOOP” then click ok.

![hadoop](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop_extract.png)

You can see, hadoop file is extracting.

![hadoop](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/extracted_hadoop.png)

**After the extraction is completed, you will get this diagnostic message, just close it**

**Step 3:** Then you will see Hadoop folder created, open it and you will find a folder named **“hadoop-2.4.1”**. Open this folder all Hadoop files are inside this folder. 

**“NOTE: Within the Hadoop folder you will find another folder hadoop-2.4.1 containing all the hadoop files. Copy all the hadoop files from hadoop-2.4.1 folder and paste it inside the main folder Hadoop so that we have all files inside the main folder. After copying delete hadoop-2.4.1 folder. Now the bin path will be C:\Hadoop\bin.”**

### EDITION OF HADOOP etc FILE:

To edit hadoop file you will need **Notepad++**.

Click on this link or copy paste the link on new tab.

https://notepad-plus.en.softonic.com/download

![notepad](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/notepad.png)

**Lets start with editing now.**

**Step 1:** Open **Hadoop** folder --> open **“etc”** folder --> Inside you will find **hadoop** folder, open it:

We are going to edit 5 files here.

1st file= **core-site** (XML DOCUMENT) right click on this file and select option **edit with notepad++**.

Page will open like this:

**NOTE: don’t forget to save all the files after you paste your values in it. Save it after editing forsure.**

![core](https://gitlab.com/Rorke540/hadopp-lab1/-/raw/master/Images/Image-1.png)

Copy and paste this value given below inside the configuration in core-site.xml file.

    
    <property>

        <name>fs.defaultFS</name>

        <value>hdfs://localhost:9000</value>

    </property>
`

After pasting it, it will look like this.

![core](https://gitlab.com/Rorke540/hadopp-lab1/-/raw/master/Images/Image-4.png)

In the same way edit hdfs-site, mapred-site, and yarn-site (XML document).

**To assist you**

Value for each file is given below.

**Hdfs-site.xml**

Before editing this file, go to Hadoop folder in C:\ drive and create a new folder named as data.

Inside data folder create 2 folders **“namenode”** and **“datanode”**. And in value you must paste **path location** of your datanode and namenode.

![data](https://gitlab.com/Rorke540/hadopp-lab1/-/raw/master/Images/Image-2.png)

**Values are:**

**Hdfs-site.xml**
   

    <property>

        <name>dfs.replication</name>

        <value>1</value>

    </property>

    <property>

        <name>dfs.namenode.name.dir</name>

        <value><path of namednode></value>

    </property>

    <property>

        <name>dfs.datanode.data.dir</name>

        <value><path of datanode></value>

    </property>



**Mapred-site.xml**

    <property>

        <name>mapreduce.framework.name</name>

        <value>yarn</value>

    </property>

**Yarn-site.xml**
    
    <property>

        <name>yarn.nodemanager.aux-services</name>

        <value>mapreduce_shuffle</value>

    </property>

    <property>

        <name>yarn.nodemanager.auxservices.mapreduce.shuffle.class</name>

        <value>org.apache.hadoop.mapred.ShuffleHandler</value>

    </property>

**Now open Hadoop-env (window command file) and edit it with notepad++.**

![hadoop cmd](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop_cmd.png)

Edit here **JAVA_HOME= “variable value"** you entered when you were creating the environmental variable for JAVA”. Open your setting and see your variable value. 

**Note: When you enter the Variable Value kindly remove the "bin".**

**REMINDER: do not forget to save your edited files.**

### SETTING UP THE ENVIRONMENT VARIABLES FOR HADOOP:

**Step 1:** Click on start and go to **settings** --> In setting go to **“system”** and then search for **“edit for environment variables”** --> Select **“environment variables”** and click ok --> Click on **“New”** in User Variables section.

Write variable name as: **“HADOOP_HOME”** and variable value is the path location of bin.

![hadoophome](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop_home.png)

**Step 2:** Then click on **“Path”** in System Variables, select **“New”** enter the variable value of Hadoop bin folder here as well.

![hadooppath](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop_path.png)

**Step 3:** Then add the path location of hadoop’s **sbin** folder as well and click ok.

![sbin](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/sbin.png)

**In Hadoop we are missing some configuration file, let’s fix that**

**Step 1:** Click on this link https://drive.google.com/file/d/1AMqV4F5ybPF4ab4CeK8B3AsjdGtQCdvy/view or copy paste this link on new tab.download it.

**Step 2:** From download folder copy this file and paste it inside the Hadoop folder.

**Step 3:** Extract this Hadoop configuration file.

![configuration](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/configuration_file.png)

**Step 4:** A new folder named as **“HadoopConfiguration-FIXbin”** is created. Open it copy that bin folder and **replace** the previous bin. Now delete unnecessary files of Hadoop configuration.

### Veryfying the installation of HADOOP(COMMAND PROMPT)

**Step 1:** Open command prompt and type **“hdfs namenode -format”** and enter.

![hadoop.cmd](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop.cmd.png)

**Step 2:** Type **start-all.cmd** and enter.

![hadoop](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop.cmd_2.png)

**All files are opening now**